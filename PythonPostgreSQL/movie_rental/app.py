from user import User
import os 
import json
from time import sleep

os.system("clear")

def menu():
    print("Welcome to your movie database")
    user = input("What is your name? ")
    filename = "{}.json".format(user)

    if file_exists(filename): 
        with open(filename, 'r') as f:
            json_data = json.load(f)
            user = User.from_json(json_data)
            print("Welcome back, {}".format(user.name))
    else:
        user = User(user)
        print("Welcome {}".format(user.name))
    while True:
        print("What would you like to do today?")
        print("1: add a movie \n2: See movie list \n3: Delete a movie \n4: Mark movie as watched \n5: Display watched movies \n6: Save & quit")
        selection = input("Selection: ")
        if selection == '1':
            name = input("What is the movies name? ")
            genre = input("What is the movies genre? ")
            user.add_movie(name, genre, False)
            print("{} was added to your list".format(name))
        elif selection == '2':
            user.print_movie_list()
        elif selection == '3':
            name = input("What movie would you like to delete? ")
            user.delete_movie(name)
        elif  selection == '4':
            name = input("What movie have you watched recently? ")
            user.movie_watched(name)
        elif selection == '5':
            print(user.watched_movies())
        elif selection == '6':
            with open("{}.json".format(user.name), 'w') as f:
                json.dump(user.json(), f)
            print("Your list has been saved")
            sleep(2)
            False
            break
        else:
            print("That is not a valid selection, try again")
            sleep(1)
            os.system("clear")
            continue
        
def file_exists(filename):
    return os.path.isfile(filename)


menu()




