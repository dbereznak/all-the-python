from movie import Movie
    
class User:
    def __init__(self, name):
        self.name = name
        self.movies = []
        
    def __repr__(self):
        return "<User {}>".format(self.name)
    
    def add_movie(self, movie_name, genre, watched):
        new_movie = Movie(movie_name, genre, watched)
        self.movies.append(new_movie)
        
    def delete_movie(self, movie_name):
        for movie in self.movies:
            if movie.name == movie_name:
                self.movies.remove(movie)
            else:
                print("Sorry, That movie is not in your list")
    
    def movie_watched(self, movie_name):
        for movie in self.movies:
            if movie.name == movie_name:
                movie.watched = True;            
    
    def watched_movies(self):
        return list(filter(lambda movie: movie.watched, self.movies))


    def json(self):
        return {
            'name' : self.name,
            'movies':  [
                movie.json() for movie in self.movies
            ]
        }
    
    @classmethod
    def from_json(cls, json_data):
        user = User(json_data["name"])
        movies = []
        for movie_data in json_data['movies']:
            movies.append(Movie.from_json(movie_data))
        user.movies = movies

        return user
    
    def print_movie_list(self):
        for movie in self.movies:
            print("Name: {} || Genre: {}".format(movie.name, movie.genre))
            