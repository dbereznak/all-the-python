from os import system

system("clear")

for num in range(1, 100):
    if num % 3 == 0 and num % 5 == 0:
        print(f"{num}: FizzBuzz....")
    elif num % 3 == 0:
        print(f"{num}: Buzz....")
    elif num % 5 == 0:
        print(f"{num}: Fizz....")
    else:
        print(num)