import os
from scraps import intro

currentDirectory = os.getcwd()
movie_list = []


def add_movie_to_list():
    movie_name = input("What is the name of the movie:").title()
    has_watched = ""
    watched = False
    while True:
        has_watched = input("Have you watched this movie(Y/N)").lower()
        if has_watched == "n":
            watched = False
            break
        elif has_watched == "y":
            watched = True
            break
        else:
            print("That is not a valid selection")

    movie_dict = {"name": movie_name, "watched": watched}
    print(f"{movie_name} has been added to the list")
    movie_list.append(movie_dict)


def print_list():
    if len(movie_list) > 0:
        for movie in movie_list:
            print(f"Movie: {movie['name']} Watched: {movie['watched']}")
    else:
        print("There are no movies in your list")


def mark_as_watched():
    pass

def user_menu():
    os.system("cls")
    choice = ""
    print(intro)
    while choice != "4":
        print("1. Add a movie to your list \n2. View your list \n3. Mark as watched \n4. Quit")
        choice = input("Choice: ")
        if choice == "1":
            add_movie_to_list()
            continue
        if choice == "2":
            print_list()
            continue
        if choice == "3":
            print("three")
            continue
        if choice == "4":
            print("Good bye, have a nice day")
            break
        else:
            print("That is not a valid choice")


user_menu()

