from os import system
import json

system("clear")
# Creates a list of friends
print("This program keeps track of friends")
your_name = input("To start off, What is your name?")
friends = []
cont = True


while cont:
    new_friend_name = input("Please enter a friends name: ").strip()
    new_friend_age = input("How old is your friend: ").strip()
    new_friend_gender = input("What gender is your friend: ").strip()
    new_friend = {"name": new_friend_name, "age": new_friend_age, "gender": new_friend_gender}
    friends.append(new_friend)
    invalid_answer = True
    while invalid_answer == True:
        repeat = input("Do you want to add another friend(Y/N)").upper()
        if repeat == "Y":
            cont = True
            invalid_answer = False
        elif repeat == "N":
            cont = False
            invalid_answer = False
        else:
            print("That was a invalid answer, try again.")
            invalid_answer = True
    

print(f"Here are all your friends, {your_name}")


for friend in friends:
    print(f"name: {friend['name']} \nage: {friend['age']} \ngender: {friend['gender']}\n")

with open("friends.txt", "w") as out:
    json.dump(friends, out, indent=4, sort_keys=True)
