from os import system
from time import sleep
from colorama import Fore
from colorama.ansi import Style

class Student:
    def __init__(self, name):
        self.name = name
        self.grades = []
        self.average = 0


system('clear')

students = []


def create_student():
    name = input("What is the students name? ")
    student_data = Student(name)
    students.append(student_data)
    return student_data
 
def add_grade(student):
    num_of_grades = int(input("How many grades would you like to enter today? "))
    for i in range(1, num_of_grades+1):
        grade = int(input("grade {}: ".format(i)))
        student.grades.append(grade)
        avg = sum(student.grades) / num_of_grades
        student.average = round(avg)

def print_student():
    name = input("What is the students name? ")
    for i, student in enumerate(students):
        if student.name == name:
            print(Fore.GREEN + "Name: {} \nAverage: {}". format(student.name, student.average))

def print_students():
    for student in students:
        print(Fore.GREEN + "Name: {} \nAverage: {}". format(student.name, student.average))

def no_students():
        print(Fore.LIGHTYELLOW_EX + "There are no students in the database")
        sleep(2)
        system('clear')

def menu():
    menu_active = True
    while menu_active:
        print(Style.RESET_ALL)
        print("Welcome to the the Sombra Corps Student Database")
        print("#"*50)
        print("1: Add Student \n2: Add Grade \n3: look at student details\n4: Look at all students\n0 input will exit out of menu")
        selection = input("What would you like to do today: ")
        if selection == '1':
            create_student()
        elif selection == '2':
            if len(students) < 1:
                no_students()
                continue
            else:
                s = input("What student do you want to add grades to? ")
                for i, student in enumerate(students):
                    if student.name == s:
                        add_grade(students[i])
        elif selection == '3':
            if len(students) < 1:
                no_students()
                continue
            else:    
                print_student()
        elif selection == '4':
            if len(students) < 1:
                no_students()
                continue
            else:    
                system('clear')
                print_students()
        elif selection == '0':
            menu_active = False
        else:
            print(Fore.RED + "That is not a valid selection")
            sleep(1)
            system('clear')
            continue    



menu()

