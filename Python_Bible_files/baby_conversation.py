import random

why = True
questions = ["Why is the sky blue? ", "Why is water wet?", "Why is fire hot", "How are babies made? "]
right_answer = "because it is"

print(random.choice(questions))

while why is True:
    answer = input("Response: ").lower().strip()
    if(answer == right_answer):
        print("Oh.. that is really neat.")
        why = False
    else:
        print("But why...?")