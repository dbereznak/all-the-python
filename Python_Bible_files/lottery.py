import random

user_values = {}
lottery_values = {} 
matching_values = set()
lottery_rolls = 5


def lottery_number():
    number= set()
    while len(number) < lottery_rolls:
        number.add(random.randint(1, 99))    
    return number

def user_input():
    temp = set()
    for number in range(lottery_rolls):
        user_number = input("Please enter number {} for your lottery pick: ".format(number + 1))
        if int(user_number) in temp:
            print("I'm sorry that number is already used.")
            user_number = input("Please enter number {} for your lottery pick: ".format(number + 1))
            temp.add(int(user_number))
        elif int(user_number) > 99 or int(user_number) < 0:
            print("Sorry your number must be between 0 and 99")
            user_number = input("Please enter number {} for your lottery pick: ".format(number + 1))
            temp.add(int(user_number))
        else:
            temp.add(int(user_number))
    return temp


print("Tonight is the big night. You might win Jeff Bezos net wealth. \nChose your lottery numbers wisely")
user_values = user_input()

lottery_values = lottery_number()

matching_values = user_values.intersection(lottery_values)

print("Your numbers were {}".format(user_values))
print("The winning numbers were {}".format(lottery_values)) 
if len(matching_values) > 0:
    print("The matching numbers were {}".format(matching_values))
    print("You won ${} dollars".format(100 ** len(matching_values)))
else:
    print("Sorry, better luck next time, no numbers matched")