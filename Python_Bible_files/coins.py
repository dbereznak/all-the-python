import random

class Coin:
    def __init__(self, rare =False):
        self.rare = rare
        if self.rare:
            self.value = 1.25
        else:        
            self.value = 1.00
        self.color = "Silver"
        self.num_edges = 1
        self.diameter = 22.5 #mm
        self.thickness = 3.15 #mm
        self.heads = True
        
    def rust(self):
        self.color = "Greenish"
        
    def clean(self):
        self.color = "Silver"
    
    def flip(self):
        heads_options = [True, False] 
        choice = random.choice(heads_options)
        self.heads = choice  
        if choice == True:
            print("The coin landed on heads")
        else:
            print("The coin landed on tails")
    
    def __del__(self):
        print("The coin has been spent")
    

coin_one = Coin()
coin_two = Coin()
coin_two.rust()
coin_one.flip()

print(coin_two.color)

