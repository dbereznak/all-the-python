films = {
    "Seven": {"Rated": "R"},
    "Die Hard": {"Rated": "R"},
    "The Labyrinth": {"Rated": "PG"},
    "Neverening Story": {"Rated": "PG"}
}

age = int(input("How old are you? ").strip())
xit = False

print("Please pick a film to watch.")
for film in films:
    print("{}".format(film))    


while xit == False:
    user_input = input("Which film would you like to see? ").strip().title()
    if user_input in films:
        access = films[user_input]["Rated"]
        if age >= 18 and access == 'R':
            print("You are old enough to see the film. \n Enjoy the show")
            xit = True
        elif age < 18 and access == 'R':
            print("sorry bud, you need {} more years to watch this film".format(18 - age))
            xit = True
        else:
            print("Enjoy the show.")
            xit = True
    else:
        print("That film is not playing")



