import random

magic_number = random.randint(0, 9)

def guess_number(guesses):
    guess = input("Guess what number I am thinking of: ")
    if int(guess) == magic_number:
        print("correct, {} was the right number".format(guess))
        print("Congratulations")
    if int(guess) != magic_number:
        print("Sorry that is not the number on my mind, {} more tries left.".format(guesses - 1))


def run_game(guesses):
    for i in range(guesses):
        guess_number(guesses)
        if i == (guesses - 1):
            print("No more tries left, you failed. \nThe magic number was {}".format(magic_number))



chances = int(input("How many chances should I give you(1 - 5)? "))
    
if int(chances) > 5:
    print("Sorry, that is way to many chances")
    chances = int(input("How many chances should I give you(1 - 5)? "))
else:
    run_game(chances)