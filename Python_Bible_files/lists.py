known_users = ["Donny", "Angela", "Kahlan", "David", "Blake"]

while True:
    print("Hi, my name is Deep thought")
    your_name = input("What is your_name? ").strip().capitalize()
    if your_name in known_users:
        print("Welcome {} ".format(your_name))
        remove = input("Would you like to be removed from the list? Y/N ").lower()
        if remove == "y":
            print("You have been removed")
            known_users.remove(your_name)
        elif remove == "n":
            print("good")
    elif your_name not in known_users:
        print("Access denied")
        add_user = input("Would you like to be added to the list? Y/N ").lower()
        if add_user == "y":
            print("User added")
            known_users.append(your_name)
        elif add_user == "n":
            False   
